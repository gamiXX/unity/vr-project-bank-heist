using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;

public class AgentController : MonoBehaviour
{
    private NavMeshAgent agentNav;
    public Transform[] destinations;
    private int currentDestinationIndex = 0;
    public float detectionRadius = 3f;
    public Material lineMaterial;
    private bool playerDetected;
    private LineRenderer lineRenderer;
    private float normalSpeed = 1.5f;
    private float boostedSpeed = 3f;

    void Start()
    {
        agentNav = GetComponent<NavMeshAgent>();
        NavigateToDestination(currentDestinationIndex);

        // Add the LineRenderer component to this game object
        lineRenderer = gameObject.AddComponent<LineRenderer>();

        // Set the material of the line renderer
        lineRenderer.material = lineMaterial;

        // Set the color and width of the line renderer
        lineRenderer.startColor = Color.yellow;
        lineRenderer.endColor = Color.yellow;
        lineRenderer.startWidth = 0.1f;
        lineRenderer.endWidth = 0.1f;

    }

    void Update()
    {
        AgentDetectionRange();
        DrawDetectionDisk();
        Check_If_Destination_Reached();
    }

    private void Check_If_Destination_Reached()
    {
        if (agentNav.remainingDistance < 0.1f && !agentNav.pathPending)
        {
            currentDestinationIndex = (currentDestinationIndex + 1) % destinations.Length;
            NavigateToDestination(currentDestinationIndex);
        }
    }
    private void AgentDetectionRange()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, detectionRadius);
        foreach (Collider collider in colliders)
        {
            if (collider.CompareTag("Player"))
            {
                Debug.Log("Player detected!");
                agentNav.SetDestination(collider.gameObject.transform.position);
                playerDetected = true;
            }
            else
            {
                playerDetected = false;
            }
        }

        if (playerDetected)
        {
            agentNav.speed = boostedSpeed;
        }
        if (!playerDetected)
        {
            agentNav.speed = normalSpeed;
        }
    }
    
    private void NavigateToDestination(int destinationIndex)
    {
        agentNav.SetDestination(destinations[destinationIndex].position);
    }

    private void DrawDetectionDisk()
    {
        float circumference = 2 * Mathf.PI * detectionRadius;
        int numSegments = Mathf.RoundToInt(circumference / 0.1f);
        numSegments = Mathf.Max(numSegments, 3);
        float angleDelta = 360f / numSegments;
        Vector3[] positions = new Vector3[numSegments + 1];
        positions[0] = transform.position;
        for (int i = 1; i <= numSegments; i++)
        {
            float angle = (i - 1) * angleDelta;
            float x = Mathf.Sin(angle * Mathf.Deg2Rad) * detectionRadius;
            float z = Mathf.Cos(angle * Mathf.Deg2Rad) * detectionRadius;
            positions[i] = transform.position + new Vector3(x, 0f, z);
        }
        lineRenderer.positionCount = positions.Length;
        lineRenderer.SetPositions(positions);
    }
}


